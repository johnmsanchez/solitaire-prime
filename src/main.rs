mod cards;
use std::io;

use cards::deck::Deck;

fn print_menu() {
    println!("\nWelcome To Solitaire Prime!");
    println!("1) New Deck");
    println!("2) Display Deck");
    println!("3) Shuffle Deck");
    println!("4) Play Solitaire Prime");
    println!("5) Exit");
    println!("Select A Number: ");
}

fn is_prime(num: i16) -> bool {
    if num <= 1 {
        return false;
    }

    for i in 2..(num/2 + 1) {
        if num % i == 0 {
            return false;
        }
    }

    true
}

fn play_solitaire(deck: &mut Deck) {
    if deck.is_cards_empty() {
        println!("Deck is empty.");
        println!("Create a new deck first!");
        return;
    }

    let mut counter = 0;
    let mut sum = 0;

    while !deck.is_cards_empty() {
        let card_value = deck.deal();
        sum += card_value;
        if is_prime(sum) {
            println!("Prime: {}", sum);
            counter += 1;
            if !deck.is_cards_empty() {
                sum = 0;
            }
        }
    }

    println!("Final Prime: {}", sum);

    if is_prime(sum) {
        println!("Winner in {} piles!", counter);
    } else {
        println!("Loser!");
    }
}

fn main() {
    let mut deck: Deck = Deck::new();

    loop {
        print_menu();
        let mut user_input = String::new();
        io::stdin()
            .read_line(&mut user_input)
            .expect("Unable to parse user input");

        let num = match user_input.trim().parse::<i16>() {
            Ok(x) => x,
            _ => 0,
        };

        println!();

        match num {
            1 => {
                deck = Deck::new();
            }
            2 => {
                println!("Current Deck:");
                deck.display();
            }
            3 => {
                deck.shuffle();
            }
            4 => {
                play_solitaire(&mut deck);
            }
            5 => {
                break;
            }
            _ => {
                println!("Enter a number from 1 to 5!");
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_new_deck_correctly() {
        let deck = Deck::new();
        assert_eq!(deck.current_cards(), 52);
    }

    #[test]
    fn deal_correctly() {
        let mut deck = Deck::new();
        let val = deck.deal();
        assert_eq!(val, 10);
        assert_eq!(deck.current_cards(), 51);
    }
}

pub struct Card {
    suit: char,
    rank: char,
}

impl Card {
    pub fn new(suit: char, rank: char) -> Self {
        Self { suit, rank }
    }

    pub fn display(&self) {
        print!("{}{} ", self.suit, self.rank);
    }

    pub fn get_value(&self) -> i16 {
        match self.rank {
            'A' => 1,
            'J' | 'Q' | 'K' | 'T' => 10,
            _ => self.rank.to_digit(10).unwrap_or(0) as i16,
        }
    }
}

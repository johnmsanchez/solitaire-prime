use std::mem;
use rand::Rng;

use super::card::Card;

const SUITS: [char; 4] = ['H', 'D', 'S', 'C'];
const RANKS: [char; 13] = [
    'A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K',
];

pub struct Deck {
    storage: Vec<Option<Card>>,
}

impl Deck {
    pub fn new() -> Self {
        let mut storage: Vec<Option<Card>> = Vec::new();

        for suit in SUITS.iter() {
            for rank in RANKS.iter() {
                let card = Card::new(*suit, *rank);
                storage.push(Some(card));
            }
        }

        Self { storage }
    }

    pub fn display(&self) {
        let mut counter: i16 = 0;
        for card in self.storage.iter() {
            if let Some(a) = card.as_ref() {
                a.display();
            }

            counter += 1;
            if counter % 13 == 0 {
                println!();
            }
        }
    }

    pub fn shuffle(&mut self) {
        let mut random = rand::thread_rng();
        for _x in 0..52 {
            let x = random.gen_range(0..52);
            let y = random.gen_range(0..52);

            let temp_card_1 = mem::replace(&mut self.storage[x], None);
            let temp_card_2 = mem::replace(&mut self.storage[y], None);

            mem::replace(&mut self.storage[x], temp_card_2);
            mem::replace(&mut self.storage[y], temp_card_1);
        }
    }

    pub fn deal(&mut self) -> i16 {
        let card = self.storage.pop().unwrap().unwrap();
        card.get_value()
    }

    pub fn is_cards_empty(&self) -> bool {
        self.storage.is_empty()
    }

    pub fn current_cards(&self) -> usize {
        self.storage.len()
    }
}

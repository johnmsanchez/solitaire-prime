# Solitaire Prime
A version of Solitaire base on prime numbers.

## Game Rules
1)	Take the top card from the deck and place it face up on the table.
2)	The Sum is now the value of that card (Ace = 1, 2 = 2, … 10 = 10, Jack = 10, Queen = 10, King = 10)
3)	If the Sum is a prime number, discard that pile, and start over at instruction #1
4)	If the Sum is not prime, take the next card from the top of the deck and place it on top of the pile on the table.
5)	The Sum is now the sum of all cards in the pile on the table.
6)	Go to instruction #3.
7)	Play the game until all cards in the deck have been played.
